use std::sync::mpsc;
use std::sync::Arc;
use std::convert::Infallible;
use std::collections::HashMap;

use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};

use futures::future::FutureExt;

use tokio::task;
use tokio::sync::mpsc as tokio_mpsc;

mod tsvetkov;

use tsvetkov::messages as msg;
use tsvetkov::route as route;
use tsvetkov::operation::Operation;
use tsvetkov::forecast_api as api;
use tsvetkov::request::{*};

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    // ManagerTask
    let (tx, rx) = mpsc::channel::<msg::RequestMessage>();

    task::spawn_blocking(move || {
        //let tasks:std::collections::HashMap<u64, >
        let mut operation_id = 0u64;
        let operations = Arc::new(tokio::sync::Mutex::new(HashMap::<u64, Operation>::new()));

        let operations_for_create = operations.clone();
        let mut create_operation = || {
            operation_id += 1;
            let id = operation_id;
            let operations = operations_for_create.clone();
            async move {
                {
                    let mut operation = operations.lock().await;
                    operation.insert(id, Operation::InProcess);
                }
                operation_id
            }
        };
        let operations_for_error = operations.clone();
        let error_operation = move || {
            let operations = operations_for_error.clone();
            move |id: u64, err: &'static str| {
                async move {
                    let mut operation = operations.lock().await;
                    if let Some(op) = operation.get_mut(&id) {
                        *op = Operation::Error { error: err.to_string() }
                    }
                }
            }
        };
        loop {
            let mut request = rx.recv().unwrap();

            println!("current thread: {:?} receive request {:?}", std::thread::current(), request);
            let mut response_sender = request.response_sender;
            match request.request_type {
                msg::RequestType::NowForecast { lat, lon } => {
                    let create_operation = create_operation();
                    let error_operation = error_operation();
                    let operations = operations.clone();
                    task::spawn(async move {
                        let operation_id = create_operation.await;

                        if let Err(_) = response_sender.send(msg::ResponseType::OperationCreated { id: operation_id }) {
                            error_operation(operation_id, REQUEST_TO_API_CANCELED).await;
                            return;
                        }

                        let result = request_to_apis(lat, lon,
                                                     |item, pos| item.today_url(pos),
                                                     |item, response| item.parse_today(response)).await;

                        if result.is_empty() {
                            error_operation(operation_id, REQUEST_TO_API_NOT_DATA).await;
                            return;
                        }

                        let count = result.len() as f64;
                        let sum = result.iter().fold(0f64, |acc, item| acc + item.temperature);
                        let average_temperature = api::round_average_temperature(sum / count);

                        let result = vec![api::OneDayForecast { date: result[0].date, temperature: average_temperature }];

                        println!("Result: {:?}", result);

                        {
                            let mut operation = operations.lock().await;
                            if let Some(op) = operation.get_mut(&operation_id) {
                                *op = Operation::Succeed { forecast: result }
                            }
                        }
                    });
                }
                msg::RequestType::FiveDaysForecast { lat, lon } => {
                    let create_operation = create_operation();
                    let error_operation = error_operation();
                    let operations = operations.clone();
                    task::spawn(async move {
                        let operation_id = create_operation.await;
                        if let Err(_) = response_sender.send(msg::ResponseType::OperationCreated { id: operation_id }) {
                            error_operation(operation_id, REQUEST_TO_API_CANCELED).await;
                            return;
                        }

                        let result = request_to_apis(lat, lon,
                                                     |item, pos| item.five_day_url(pos),
                                                     |item, response| item.parse_five_days(response)).await;

                        if result.is_empty() {
                            error_operation(operation_id, REQUEST_TO_API_NOT_DATA).await;
                            return;
                        }

                        let today = chrono::Utc::today().and_hms(0, 0, 0);
                        let forecast_end = today + chrono::Duration::days(5);

                        let mut r = HashMap::new();

                        for item in result {
                            if item.date >= today && item.date < forecast_end {
                                r.entry(item.date).and_modify(|v: &mut api::OneDayForecast| {
                                    v.temperature = api::round_average_temperature((v.temperature + item.temperature) / 2f64)
                                }).or_insert(item);
                            }
                        }

                        let result = r.into_iter().map(|v| v.1).collect::<Vec<_>>();
                        println!("5 days: {:?}", result);

                        {
                            let mut operation = operations.lock().await;
                            if let Some(op) = operation.get_mut(&operation_id) {
                                *op = Operation::Succeed { forecast: result }
                            }
                        }
                    });
                }
                msg::RequestType::StatusOperation { id } => {
                    let operations = operations.clone();
                    task::spawn(async move {
                        let response = {
                            let operations = operations.lock().await;
                            match operations.get(&id) {
                                Some(v) => { msg::ResponseType::OperationStatus(v.clone()) }
                                None => { msg::ResponseType::Error { error: "Operation not found".to_string() } }
                            }
                        };
                        response_sender.send(response);
                    });
                }
            }
        }
    });


    let make_svc = make_service_fn(move |_conn| {
        let tx = tx.clone();
        async move {
            Ok::<_, Infallible>(service_fn(move |request: Request<Body>| {
                let tx = tx.clone();
                async move {
                    let request_type = route::request_type(request.uri());

                    if let Err(e) = request_type {
                        return route::error_response(e);
                    }

                    let (response_tx, mut response_rx) = tokio_mpsc::unbounded_channel();

                    if let Err(_) = tx.send(msg::RequestMessage {
                        request_type: request_type.unwrap(),
                        response_sender: response_tx,
                    }) {
                        return route::internal_error_response_with("Unable to send request.");
                    }

                    response_rx.recv().then(|r| async move {
                        if let None = r {
                            return route::internal_error_response();
                        }
                        //println!("receive: {:?}", r.unwrap());
                        let result: Result<Response<Body>, Infallible> =
                            Ok(Response::new(Body::from(serde_json::to_string(&r).unwrap())));
                        result
                    }).await
                }
            }))
        }
    });

    let addr = ([127, 0, 0, 1], 3000).into();

    let server = Server::bind(&addr).serve(make_svc);

    println!("Listening on http://{}", addr);

    server.await?;

    Ok(())
}

