use chrono::Utc;
use serde::Serialize;
use chrono::serde::ts_seconds;

const PARSE_RESPONSE_FAILED: &str = "Parse response failed.";

#[derive(Clone, Copy, Debug, Serialize)]
pub struct OneDayForecast {
    #[serde(with = "ts_seconds")]
    pub date: chrono::DateTime<Utc>,
    pub temperature: f64,
}

//fn str_from_date<S>(chrono_date: &chrono::Date<Utc>, serelizer: S) -> Result<S::Ok, S::Error>
//    where S: serde::Serializer
//{
//    let result = chrono_date.and_time(chrono::NaiveTime::from_hms(0,0,0));
////    if None == result {
////        return  de::Error::custom("Date is wrong.");
////    }
//    serelizer.serialize_i64(result.unwrap().timestamp())
//}

#[derive(Clone)]
pub struct DarkSky {
    pub key: String,
}

#[derive(Clone)]
pub struct AccuWeather {
    pub key: String,
}

pub trait KeyApi {
    fn key_api(&self) -> &str;
}

#[derive(Clone)]
pub enum PositionApi {
    CityUrl(String),
    Coordinate(String),
}

pub fn round_average_temperature(t: f64) -> f64 {
    (t * 100f64).round() / 100f64
}

pub trait ForecastApi: KeyApi {
    fn coordinates(&self, lat: f64, lon: f64) -> PositionApi;
    fn today_url(&self, coordinate_chunk: &str) -> String;
    fn five_day_url(&self, coordinate_chunk: &str) -> String;

    fn parse_position_api(&self, response_body: &str) -> Result<String, String> {
        Ok(response_body.to_string())
    }
    fn parse_today(&self, response_body: &str) -> Result<Vec<OneDayForecast>, String>;
    fn parse_five_days(&self, response_body: &str) -> Result<Vec<OneDayForecast>, String>;
}

impl KeyApi for AccuWeather {
    fn key_api(&self) -> &str {
        self.key.as_str()
    }
}

impl ForecastApi for AccuWeather {
    fn coordinates(&self, lat: f64, lon: f64) -> PositionApi {
        PositionApi::CityUrl(
            format!("http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey={}&q={},{}",
                    self.key_api(), lat, lon).to_string())
    }

    fn today_url(&self, coordinate_chunk: &str) -> String {
        format!("http://dataservice.accuweather.com/forecasts/v1/daily/1day/{}?apikey={}&metric=true",
                coordinate_chunk, self.key_api()).to_string()
    }

    fn five_day_url(&self, coordinate_chunk: &str) -> String {
        format!("http://dataservice.accuweather.com/forecasts/v1/daily/5day/{}?apikey={}&metric=true",
                coordinate_chunk, self.key_api()).to_string()
    }

    fn parse_position_api(&self, response_body: &str) -> Result<String, String> {
        let json = serde_json::from_str::<serde_json::Value>(response_body)
            .map_err(|err| {
                format!("{} {}", PARSE_RESPONSE_FAILED, err)
            })?;
        json["Key"].as_str()
            .ok_or(format!("{} {}", PARSE_RESPONSE_FAILED, "Key not found"))
            .map(|v| v.to_string())
    }

    fn parse_today(&self, response_body: &str) -> Result<Vec<OneDayForecast>, String> {
        let result = self.parse_five_days(response_body)?;
        if result.len() == 1 {
            return Ok(result);
        }
        Err(format!("{}", PARSE_RESPONSE_FAILED))
    }

    fn parse_five_days(&self, response_body: &str) -> Result<Vec<OneDayForecast>, String> {
        let j: serde_json::Value = serde_json::from_str(response_body)
            .map_err(|err| format!("{} {}", PARSE_RESPONSE_FAILED, err))?;

        let result = match &j["DailyForecasts"] {
            serde_json::Value::Array(a) => {
                let r = a.iter().map(|v| -> Result<OneDayForecast, &str>{
                    let c_date = match v["Date"].as_str() {
                        Some(v) => match v.parse::<chrono::DateTime<chrono::Utc>>() {
                            Ok(d) => Ok(d.date()),
                            Err(_) => Err("Date wrong format"),
                        }
                        None => Err("Date not found"),
                    }?;

                    let c_temperature = match &v["Temperature"] {
                        serde_json::Value::Object(day) => {
                            let numbers = day.iter().map(|(_, v)| {
                                match &v["Value"] {
                                    serde_json::Value::Number(v) => {
                                        match v.as_f64() {
                                            Some(n) => Some(n),
                                            _ => None,
                                        }
                                    }
                                    _ => None,
                                }
                            }).collect::<Vec<_>>();

                            if !numbers.iter().all(|v| v.is_some()) {
                                Err("Temperature is not number")
                            } else {
                                Ok(round_average_temperature(numbers.iter().map(|v| v.unwrap()).fold(0f64, |acc, x| acc + x) /
                                    numbers.len() as f64))
                            }
                        }
                        _ => Err("Temperature not found"),
                    }?;
                    Ok(OneDayForecast { date: c_date.and_hms(0,0,0), temperature: c_temperature })
                }).collect::<Vec<_>>();
                Ok(r)
            }
            _ => { Err("Json wrong") }
        }.map_err(|e| format!("{} {}", PARSE_RESPONSE_FAILED, e))?;

        if !result.iter().all(|v| v.is_ok()) {
            return Err(format!("{}", PARSE_RESPONSE_FAILED));
        }

        Ok(result.into_iter().map(|v| v.unwrap()).collect::<Vec<_>>())
    }
}


impl KeyApi for DarkSky {
    fn key_api(&self) -> &str {
        self.key.as_str()
    }
}

impl ForecastApi for DarkSky {
    fn coordinates(&self, lat: f64, lon: f64) -> PositionApi {
        PositionApi::Coordinate(format!("{},{}", lat, lon).to_string())
    }

    fn today_url(&self, coordinate_chunk: &str) -> String {
        format!("https://api.darksky.net/forecast/{}/{}?exclude=minutely,hourly,alerts&units=si",
                self.key_api(), coordinate_chunk)
    }

    fn five_day_url(&self, coordinate_chunk: &str) -> String {
        format!("https://api.darksky.net/forecast/{}/{}?exclude=minutely,hourly,alerts&units=si",
                self.key_api(), coordinate_chunk)
    }

    fn parse_today(&self, response_body: &str) -> Result<Vec<OneDayForecast>, String> {
        let five_day = self.parse_five_days(response_body)?;
        let today = chrono::Utc::today().and_hms(0, 0,0 );
        let today = five_day.into_iter().find(|item| item.date == today)
            .ok_or(format!("{}", PARSE_RESPONSE_FAILED))?;
        Ok(vec![today])
    }

    fn parse_five_days(&self, response_body: &str) -> Result<Vec<OneDayForecast>, String> {
        let j: serde_json::Value = serde_json::from_str(response_body)
            .map_err(|err| format!("{} {}", PARSE_RESPONSE_FAILED, err))?;
        let result = match &j["daily"] {
            serde_json::Value::Object(o) => match &o["data"] {
                serde_json::Value::Array(a) => {
                    let r = a.iter().map(|v| {
                        let temp_or_err = |v: &serde_json::Value| -> Result<f64, &str>{
                            match v {
                                serde_json::Value::Number(n) => {
                                    match n.as_f64() {
                                        Some(n) => Ok(n),
                                        _ => Err("Temperature is not number"),
                                    }
                                }
                                _ => Err("Temperature is not number"),
                            }
                        };

                        let min = temp_or_err(&v["temperatureMin"])?;
                        let max = temp_or_err(&v["temperatureMax"])?;

                        let c_date = match &v["time"] {
                            serde_json::Value::Number(n) => {
                                match n.as_i64() {
                                    Some(s) => {
                                        Ok(chrono::DateTime::<chrono::Utc>::from_utc(chrono::NaiveDateTime::from_timestamp(s, 0), chrono::Utc))
                                    }
                                    _ => Err("Not date")
                                }
                            }
                            _ => Err("Not date"),
                        }?;

                        let c_temperature = round_average_temperature((min + max) / 2f64);

                        Ok(OneDayForecast { date: c_date.date().and_hms(0,0,0), temperature: c_temperature })
                    }).collect::<Vec<Result<_, String>>>();
                    Ok(r)
                }
                _ => Err("Data not found"),
            },
            _ => Err("Daily not found"),
        }.map_err(|e| format!("{} {}", PARSE_RESPONSE_FAILED, e))?;

        if !result.iter().all(|v| v.is_ok()) {
            return Err(format!("{}", PARSE_RESPONSE_FAILED));
        }

        Ok(result.into_iter().map(|v| v.unwrap()).collect::<Vec<_>>())
    }
}
