use tokio::sync::mpsc as tokio_mpsc;
use serde::Serialize;

use crate::tsvetkov::operation::{*};

#[derive(Debug)]
pub enum RequestType {
    NowForecast{lat: f64, lon: f64},
    FiveDaysForecast{lat: f64, lon: f64},
    StatusOperation{id: u64},
}

#[derive(Debug, Serialize)]
pub enum ResponseType{
    OperationCreated{id: u64},
    OperationStatus(Operation),
    Error{ error: String},
}

#[derive(Debug)]
pub struct RequestMessage {
    pub request_type: RequestType,
    pub response_sender: tokio_mpsc::UnboundedSender<ResponseType>,
}