use crate::tsvetkov::forecast_api::{*};
use serde::Serialize;

#[derive(Debug, Clone, Serialize)]
pub enum Operation{
    Succeed{ forecast: Vec<OneDayForecast>},
    InProcess,
    Error{ error: String},
}