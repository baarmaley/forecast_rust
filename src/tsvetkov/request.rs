use crate::tsvetkov::forecast_api::{*};
use std::sync::Arc;


pub const REQUEST_TO_API_FAILED: &str = "Request to remote api failed.";
pub const REQUEST_TO_API_CANCELED: &str = "Request to remote api canceled.";
pub const REQUEST_TO_API_NOT_DATA: &str = "Not data from remote API";

async fn request_to_remote_api(url: &str) -> Result<String, String> {
    let uri = url.parse::<hyper::Uri>();
    let bytes = uri.map(|uri| {
        let scheme = uri.scheme();
        if scheme == Some(&hyper::http::uri::Scheme::HTTPS) {
            let https = hyper_tls::HttpsConnector::new();
            let client = hyper::Client::builder().build::<_, hyper::Body>(https);
            client.get(uri)
        } else {
            let client = hyper::Client::new();
            client.get(uri)
        }
    })
        .map_err(|err_uri| format!("{} ErrUri: {}", REQUEST_TO_API_FAILED, err_uri).to_string())?
        .await
        .map(|response_result| {
            println!("Response: {}", response_result.status());
            println!("Headers: {:#?}\n", response_result.headers());
            hyper::body::to_bytes(response_result.into_body())
        })
        .map_err(|err_get| format!("{} ErrGet: {}", REQUEST_TO_API_FAILED, err_get).to_string())?
        .await
        .map_err(|err_parse| format!("{} ErrParse: {}", REQUEST_TO_API_FAILED, err_parse).to_string())?;

    let result = std::str::from_utf8(bytes.as_ref())
        .map_err(|err_utf8| format!("{} ErrParse: {}", REQUEST_TO_API_FAILED, err_utf8).to_string())?;

    Ok(result.to_string())
}

async fn coordinate_convert(pos: PositionApi) -> Result<String, String> {
    match pos {
        PositionApi::CityUrl(url) => {
            // parser
            request_to_remote_api(url.as_str()).await
        }
        PositionApi::Coordinate(url) => {
            Result::<String, String>::Ok(url)
        }
    }
}

pub async fn request_to_apis<UF, PF>(lat: f64, lon: f64, url: UF, parse: PF) -> Vec<OneDayForecast>
    where UF: Fn(&(dyn ForecastApi + Send + Sync), &str) -> String + Copy,
          PF: Fn(&dyn ForecastApi, &str) -> Result<Vec<OneDayForecast>, String> + Copy
{
    let apis: Vec<Arc<dyn ForecastApi + Send + Sync>> = vec![
        Arc::new(DarkSky { key: "key to api".to_string() }),
        Arc::new(AccuWeather { key: "key to api".to_string() })];

    let requests = apis.iter().map(
        |item| {
            let item = item.clone();
            async move {
                let pos = coordinate_convert(item.coordinates(lat, lon)).await?;
                let pos_chunk = item.parse_position_api(pos.as_str())?;
                let response = request_to_remote_api(url(item.as_ref(), pos_chunk.as_str()).as_str()).await?;
                parse(item.as_ref(), response.as_str())
            }
        }).collect::<Vec<_>>();

    let results = futures::future::join_all(requests).await;

    let result = results.into_iter()
        .filter(|item| item.is_ok())
        .map(|item| item.unwrap())
        .flatten()
        .collect::<Vec<_>>();
    result
}