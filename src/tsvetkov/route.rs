use std::convert::Infallible;
use std::collections::HashMap;

use crate::tsvetkov::messages::{*};

use hyper::{Body, Response};

use serde::Serialize;

const RESOURCE_NOT_FOUND: &str = "Resource not found";
const INCOMPLECT_OR_INVALID_REQUEST: &str = "The request does not have all required attributes or has invalid attributes";
const INTERNAL_SERVER_ERROR: &str = "Internal server error. ";

#[derive(Clone, Debug, Serialize)]
struct InternalError { error: String }

#[derive(Clone, Debug, Serialize)]
struct UserError { error: String }

fn parse_coordinates(query: &str) -> Result<(f64, f64), &str> {
    let coordinates_result: Result<HashMap<_, _>, _> = query
        .split('&')
        .map(|kv| kv.split('=').collect::<Vec<&str>>())
        .map(|vec| {
            if vec.len() == 2 {
                let value = vec[1].to_string().parse::<f64>().map_err(|_| { "lat or lon not number" });
                value?;
                Ok((vec[0], value.unwrap()))
            } else {
                Err("The coordinates are incorrect")
            }
        })
        .collect();
    let coordinates = coordinates_result?;
    let lat = coordinates.get("lat").ok_or("lat not found")?;
    let lon = coordinates.get("lon").ok_or("lon not found")?;
    Ok((*lat, *lon))
}

pub fn error_response(e: &str) -> Result<Response<Body>, Infallible> {
    let user_error = UserError{error: e.to_string()};
    let json = serde_json::to_string(&user_error).unwrap_or_default();
    println!("T: {}", json);
    let mut response = Response::new(Body::from(json));
    *response.status_mut() = hyper::StatusCode::from_u16(404u16).unwrap();
    Ok(response)
}

pub fn internal_error_response() -> Result<Response<Body>, Infallible> {
    internal_error_response_with("")
}

pub fn internal_error_response_with(e: &str) -> Result<Response<Body>, Infallible> {
    let internal_error = InternalError{error: format!("{} {}", INTERNAL_SERVER_ERROR, e)};
    let json = serde_json::to_string(&internal_error).unwrap_or_default();
    let mut response = Response::new(Body::from(json));
    *response.status_mut() = hyper::StatusCode::from_u16(500u16).unwrap();
    Ok(response)
}

pub fn request_type(uri: &hyper::http::Uri) -> Result<RequestType, &str> {
    let path = uri.path()
        .split('/')
        .filter(|chunk| chunk != &"")
        .collect::<Vec<_>>();

    if path.len() == 0 {
        return Err(RESOURCE_NOT_FOUND);
    }

    let mut path_iter = path.iter();

    let mut get_next_level = move || -> Result<String, &str> {
        let next = path_iter.next().ok_or(RESOURCE_NOT_FOUND)?;
        Ok(next.to_lowercase())
    };

    let root_level = get_next_level()?;

    let result = match root_level.as_str() {
        "forecast" => {
            let query = uri.query().ok_or(INCOMPLECT_OR_INVALID_REQUEST)?;
            let coordinates = parse_coordinates(query)?;

            let next_level = get_next_level()?;
            match next_level.as_str() {
                "today" => Ok(RequestType::NowForecast { lat: coordinates.0, lon: coordinates.1 }),
                "5days" => Ok(RequestType::FiveDaysForecast { lat: coordinates.0, lon: coordinates.1 }),
                _ => Err(RESOURCE_NOT_FOUND),
            }
        }
        "operation" => {
            let operation_id = get_next_level()?
                .parse::<u64>()
                .map_err(|_| { INCOMPLECT_OR_INVALID_REQUEST })?;
            Ok(RequestType::StatusOperation { id: operation_id })
        }
        _ => Err(RESOURCE_NOT_FOUND)
    };

    result
}